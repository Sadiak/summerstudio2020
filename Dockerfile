FROM intelaipg/intel-optimized-tensorflow:2.0.0-mkl-py3

RUN apt-get update && apt-get install -y vim python3-pip pandoc

RUN apt-get install -y libsm6 libxext6 libxrender-dev

RUN echo "debconf debconf/frontend select Noninteractive" | debconf-set-selections && \
	apt-get install -y --no-install-recommends python3-tk

RUN pip install --upgrade pip

RUN pip install jupyterlab Pillow matplotlib 

RUN pip install opencv-python

WORKDIR /root/

COPY intro_to_nn /root/intro_to_nn

COPY quiz /root/quiz
